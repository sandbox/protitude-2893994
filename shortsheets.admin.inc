<?php
/**
 * @file
 * shortsheets.admin.inc
 * Code for shortsheets setup form,
 * moved into a separate file for efficiency.
 */

/**
 * Form settings for module.
 */
function shortsheets_settings($form) {
  $form = array();
  $form['description'] = array(
    '#type' => 'markup',
    '#markup' => t('Shortsheets module allows you to insert a google spreadsheet into your nodes using a shortcode. By doing this the data is saved into the database, in order to keep the table up to date the node will need to be resaved whenever there are changes to your google document. Please set the frequency you want Drupal to check for updates and how many nodes to update at a time if you have many nodes.'),
  );
  $form['shortsheets_cron_freq'] = array(
    '#type' => 'textfield',
    '#title' => t('Cron frequency (hours)'),
    '#default_value' => variable_get('shortsheets_cron_freq'),
    '#maxlength' => 100,
    '#description' => t('How frequently do you want to check for changes to shortsheets.'),
    '#element_validate' => array('shortsheets_element_validate_number')
  );
  $form['shortsheets_cron_nodes'] = array(
    '#type' => 'textfield',
    '#title' => t('Cron total nodes'),
    '#default_value' => variable_get('shortsheets_cron_nodes'),
    '#maxlength' => 100,
    '#description' => t('How many nodes to update per cron run.'),
    '#element_validate' => array('shortsheets_element_validate_number')
  );
  return system_settings_form($form);
}

function shortsheets_element_validate_number($element, &$form_state) {
  $value = $element['#value'];
  if ($value != '' && !is_numeric($value)) {
    form_error($element, t('%name must be a number.', array('%name' => $element['#title'])));
  }
}

